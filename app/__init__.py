from flask import Flask, jsonify, make_response, request
from flask_sqlalchemy import SQLAlchemy

# local imports
from config import app_config
from flask_migrate import Migrate
from flask_cors import CORS
import os

# db variable initialization
db = SQLAlchemy()


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')
    db.init_app(app)
    CORS(app)


    # temporary route
    @app.route('/')
    def hello_world():
        return 'Hello, World!'

    from app.models import Movies
    from app.models import Images


    @app.route('/addMovies',methods=['POST'])
    def addMovies():
    	name = request.form['name']
    	code = request.form['code']
    	description = request.form['description']
    	genra = request.form['genra']
    	movies = Movies(name=name, code=code, description=description,genra=genra)
    	movies.save()
    	return jsonify({"status":200})

    @app.route('/getMovies',methods=['GET'])
    def getMovies():
    	allm = Movies.query.all()
    	results = []
    	for movie in allm:
    		obj = {
    			'id':movie.id,
    			'name':movie.name,
    			'code':movie.code,
    			'description':movie.description,
    			'genra':movie.genra
    		}
    		results.append(obj)
    	response = jsonify(results)
    	response.status_code = 200
    	return response

    @app.route('/getImages',methods=['POST'])
    def getImages():
    	movies = request.form['movies']
    	images = Images.query.filter_by(movies=movies).all()
    	results = []
    	for img in images:
    		obj = {
    			'id':img.id,
    			'path':img.path,
    			'movies':img.movies
    		}
    		results.append(obj)
    	response = jsonify(results)
    	response.status_code = 200
    	return response

    @app.route('/uploadImage',methods=['POST'])
    def uploadImage():
    	# check if the post request has the file part
        if 'file' not in request.files:
             return jsonify({"status":401})
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            return jsonify({"status":401})
        if True:
            filename = file.filename
            file.save(os.path.join('c:/hola', filename))
            return jsonify({"status":200,"filename":filename})

    @app.route('/saveImage',methods=['POST'])
    def saveImage():
        idmovie = request.form['movies']
        path = request.form['path']
        images = Images(path=path,movies=idmovie)
        images.save()
        return jsonify({"status":200})


    migrate = Migrate(app, db)

    from app import models

    return app