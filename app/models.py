from app import db

class Movies(db.Model):
    """
    Create an Movies table
    """

    __tablename__ = 'movies'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(60), index=True, unique=True)
    name = db.Column(db.String(60), index=True, unique=True)
    description = db.Column(db.String(60), index=True)
    genra = db.Column(db.String(60), index=True)


    def save(self):
    	db.session.add(self)
    	db.session.commit()

class Images(db.Model):
    """
    Create a Images table
    """

    __tablename__ = 'images'

    id = db.Column(db.Integer, primary_key=True,index=True)
    path = db.Column(db.String(60), unique=True,index=True)
    movies = db.Column(db.Integer,index=True)

    def save(self):
        db.session.add(self)
        db.session.commit()